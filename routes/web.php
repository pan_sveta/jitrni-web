<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index');
Route::get('historie', 'PublicController@history');
Route::get('jitrni-v-cislech', 'PublicController@numbers');
Route::get('aktuality', 'PublicController@actualities');
Route::get('aktualita/{post}', 'PublicController@actuality');
Route::get('akce/{event}', 'PublicController@event');
Route::get('galerie', 'PublicController@galleries');
Route::get('galerie/{gallery}', 'PublicController@gallery');
Route::get('kontakty', 'PublicController@contact');
Route::get('ke-stazeni', 'PublicController@downloads');
Route::get('ke-stazeni/{asset}', 'PublicController@download');
Route::get('skolni-jidelna', 'PublicController@cafeteria');
Route::get('kariera', 'PublicController@career');

Route::get('administrace', 'AdminController@index');


Route::get('administrace/novinky', 'PostController@index');
Route::get('administrace/novinky/nova', 'PostController@create');
Route::post('administrace/novinky', 'PostController@store');
Route::get('administrace/novinky/{post}', 'PostController@show');
Route::get('administrace/novinky/{post}/upravit', 'PostController@edit');
Route::patch('administrace/novinky/{post}', 'PostController@update');
Route::get('administrace/novinky/{post}/odstranit', 'PostController@destroy');
Route::post('administrace/novinky/nahrat-obrazek', 'PostController@upload');

Route::get('administrace/akce', 'EventController@index');
Route::get('administrace/akce/nova', 'EventController@create');
Route::post('administrace/akce', 'EventController@store');
Route::get('administrace/akce/{event}', 'EventController@show');
Route::get('administrace/akce/{event}/upravit', 'EventController@edit');
Route::patch('administrace/akce/{event}', 'EventController@update');
Route::get('administrace/akce/{event}/odstranit', 'EventController@destroy');

Route::get('administrace/galerie', 'GalleryController@index');
Route::get('administrace/galerie/nova', 'GalleryController@create');
Route::post('administrace/galerie', 'GalleryController@store');
Route::get('administrace/galerie/{gallery}', 'GalleryController@show');
Route::get('administrace/galerie/{gallery}/upravit', 'GalleryController@edit');
Route::patch('administrace/galerie/{gallery}', 'GalleryController@update');
Route::get('administrace/galerie/{gallery}/odstranit', 'GalleryController@destroy');
Route::get('administrace/galerie/{gallery}/nahrat', 'GalleryController@fill');
Route::post('administrace/galerie/{gallery}/nahrat', 'GalleryController@upload');

Route::get('administrace/soubory', 'AssetController@index');
Route::get('administrace/soubory/novy', 'AssetController@create');
Route::post('administrace/soubory', 'AssetController@store');
Route::get('administrace/soubory/{asset}', 'AssetController@show');
Route::get('administrace/soubory/{asset}/upravit', 'AssetController@edit');
Route::patch('administrace/soubory/{asset}', 'AssetController@update');
Route::get('administrace/soubory/{asset}/odstranit', 'AssetController@destroy');

Route::get('administrace/skupiny-souboru', 'AssetGroupController@index');
Route::get('administrace/skupiny-souboru/nova', 'AssetGroupController@create');
Route::post('administrace/skupiny-souboru', 'AssetGroupController@store');
Route::get('administrace/skupiny-souboru/{assetGroup}/upravit', 'AssetGroupController@edit');
Route::patch('administrace/skupiny-souboru/{assetGroup}', 'AssetGroupController@update');
Route::get('administrace/skupiny-souboru/{assetGroup}/odstranit', 'AssetGroupController@destroy');

Route::get('administrace/obrazky/{image}/odstranit', 'ImageController@destroy');

Auth::routes();
