@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - '.$post->title)

@section('content')
    <a class="btn btn-primary float-right" href="{{action('PostController@edit',$post)}}">Upravit</a>
    <h1 class="display-4">{{$post->title}}</h1>
    {!! $post->content !!}
    <br>
    <footer class="blockquote-footer">Autor - {{$post->user->name}}</footer>
    <footer class="blockquote-footer">Vytvořeno - {{$post->created_at->format('d.m. Y')}} ({{$post->created_at->diffForHumans()}})</footer>
    <footer class="blockquote-footer">Upraveno - {{$post->updated_at->format('d.m. Y')}} ({{$post->created_at->diffForHumans()}})</footer>
@endsection
