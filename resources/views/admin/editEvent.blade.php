@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - Vytvořit novinku')

@section('head')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({
            selector: '#contentTextarea',
            height: '400',
        });
    </script>
@endsection

@section('content')
    <h2>Vytvořit akci</h2>
    <form method="post" action="{{action('EventController@store')}}" class="needs-validation">
        @csrf
        <div class="form-group">
            <label for="titleInput">Název</label>
            <input type="text" name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : '' }}"
                   id="titleInput" aria-describdby="name" value="{{$event->name}}" required max="50">
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        </div>
        <div class="form-group">
            <label for="titleInput">Datum</label>
            <input type="date" name="date" class="form-control {{$errors->has('date') ? 'is-invalid' : '' }}"
                   id="titleInput" aria-describdby="date" value="{{$event->date}}" required>
            <div class="invalid-feedback">{{ $errors->first('date') }}</div>
        </div>
        <div class="form-group">
            <label for="titleInput">Datum</label>
            <input type="time" name="time" class="form-control {{$errors->has('time') ? 'is-invalid' : '' }}"
                   id="titleInput" aria-describdby="time" value="{{$event->time}}" required>
            <div class="invalid-feedback">{{ $errors->first('time') }}</div>
        </div>
        <div class="form-group">
            <label for="contentTextarea">Popis</label>
            <textarea id="contentTextarea" name="xcontent">{{$event->content}}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Vytvořit</button>
    </form>
@endsection
