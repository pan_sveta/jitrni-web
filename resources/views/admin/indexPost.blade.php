@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - Novinky')

@section('content')
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nadpis</th>
            <th scope="col">Autor</th>
            <th scope="col">Vytvořeno</th>
            <th scope="col">Upraveno</th>
            <th scope="col">Akce</th>
        </tr>
        </thead>
        <tbody>
        @foreach($posts as $post)
            <tr>
                <th scope="row">{{$post->id}}</th>
                <td>{{$post->title}}</td>
                <td>{{$post->user->name}}</td>
                <td>{{$post->created_at->format('d.m. Y')}} ({{$post->created_at->diffForHumans()}})</td>
                <td>{{$post->updated_at->format('d.m. Y')}} ({{$post->created_at->diffForHumans()}})</td>
                <td>
                    <a class="btn btn-primary" href="{{action('PostController@show',$post)}}">Náhled</a>
                    <a class="btn btn-secondary" href="{{action('PostController@edit',$post)}}">Upravit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
