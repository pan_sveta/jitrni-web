@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - Skupiny souborů')

@section('content')
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Název</th>
            <th scope="col">Vytvořeno</th>
            <th scope="col">Upraveno</th>
            <th scope="col">Akce</th>
        </tr>
        </thead>
        <tbody>
        @foreach($assetGroups as $assetGroup)
            <tr>
                <th scope="row">{{$assetGroup->id}}</th>
                <td>{{$assetGroup->name}}</td>
                <td>{{$assetGroup->created_at->format('d.m. Y')}} ({{$assetGroup->created_at->diffForHumans()}})</td>
                <td>{{$assetGroup->updated_at->format('d.m. Y')}} ({{$assetGroup->created_at->diffForHumans()}})</td>
                <td>
                    <a class="btn btn-danger" href="{{action('AssetGroupController@destroy',$assetGroup)}}">Odstranit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
