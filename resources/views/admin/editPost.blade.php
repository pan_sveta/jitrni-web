@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - Upravit novinku č. '.$post->id)

@section('head')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({
            selector: '#contentTextarea',
            height: '500',
        });
    </script>
@endsection

@section('content')
    <a class="btn btn-danger float-right" href="{{ route('logout') }}"
       onclick="event.preventDefault(); document.getElementById('delete-form').submit();">Odstranit</a>
    <form action="{{action('PostController@destroy', $post)}}" id="delete-form" method="post">
        @csrf
        @method('delete')
    </form>
    <h2>Upravit novinku č. {{$post->id}}</h2>
    <form method="post" action="{{action('PostController@update', $post)}}" class="needs-validation">
        @csrf
        @method('patch')
        <div class="form-group">
            <label for="titleInput">Titulek</label>
            <input type="text" name="title" class="form-control {{$errors->has('title') ? 'is-invalid' : '' }}"
                   id="titleInput" aria-describdby="title" value="{{$post->title}}" required max="50">
            <div class="invalid-feedback">{{ $errors->first('title') }}</div>
        </div>
        <div class="form-group">
            <label for="contentTextarea">Obsah novinky</label>
            <textarea id="contentTextarea" name="xcontent">{{$post->content}}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Uložit</button>
    </form>
@endsection
