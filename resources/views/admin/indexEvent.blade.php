@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - Novinky')

@section('content')
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Název</th>
            <th scope="col">Autor</th>
            <th scope="col">Vytvořeno</th>
            <th scope="col">Upraveno</th>
            <th scope="col">Akce</th>
        </tr>
        </thead>
        <tbody>
        @foreach($events as $event)
            <tr>
                <th scope="row">{{$event->id}}</th>
                <td>{{$event->name}}</td>
                <td>{{$event->user->name}}</td>
                <td>{{$event->created_at->format('d.m. Y')}} ({{$event->created_at->diffForHumans()}})</td>
                <td>{{$event->updated_at->format('d.m. Y')}} ({{$event->created_at->diffForHumans()}})</td>
                <td>
                    <a class="btn btn-primary" href="{{action('EventController@show',$event)}}">Náhled</a>
                    <a class="btn btn-secondary" href="{{action('EventController@edit',$event)}}">Upravit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
