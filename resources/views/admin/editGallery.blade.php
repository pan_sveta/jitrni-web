@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - Novinky')

@section('content')
<div class="row">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Jméno</th>
            <th scope="col">Náhled</th>
            <th scope="col">Akce</th>
        </tr>
        </thead>
        <tbody>
        @foreach($images as $image)
            <tr>
                <th scope="row">{{$image->id}}</th>
                <td>{{$image->name}}</td>
                <td><img class="table-image" src="{{Storage::url($image->url)}}" alt=""></td>
                <td>
                    <a class="btn btn-danger" href="{{action('ImageController@destroy',$image)}}">Odstranit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
