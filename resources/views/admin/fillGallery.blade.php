@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - O nás')

@section('head')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>

@endsection

@section('content')
    <form action="{{action('GalleryController@upload', $gallery)}}" class="dropzone" id="my-awesome-dropzone">
        @csrf
    </form>
@endsection
