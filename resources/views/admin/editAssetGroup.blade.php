@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - Upravit skupinu souborů)


@section('content')
    <h2>Upravit novinku č. {{$assetGroup->id}}</h2>
    <form method="post" action="{{action('AssetGroupController@update', $assetGroup)}}" class="needs-validation">
        @csrf
        @method('patch')
        <div class="form-group">
            <label for="nameInput">Titulek</label>
            <input type="text" name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : '' }}"
                   id="nameInput" aria-describdby="name" value="{{$assetGroup->name}}" required max="50">
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        </div>
        <button type="submit" class="btn btn-primary">Uložit</button>
    </form>
@endsection
