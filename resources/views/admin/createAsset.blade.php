@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - Přidat soubor')

@section('content')
    <h2>Přidat soubor</h2>
    <form method="post" action="{{action('AssetController@store')}}" enctype="multipart/form-data"
          class="needs-validation">
        @csrf
        <div class="form-group">
            <label for="nameInput">Název</label>
            <input type="text" name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : '' }}"
                   id="nameInput" aria-describdby="name" value="{{old('name')}}" required max="50">
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        </div>
        <div class="form-group">
            <label for="assetGroupInput">Skupina souborů</label>
            <select name="assetGroup" class="form-control {{$errors->has('assetGroup') ? 'is-invalid' : '' }}"
                    id="assetGroupInput" aria-describdby="assetGroup" required>
                @foreach($assetGroups as $assetGroup)
                    <option value="{{$assetGroup->id}}">{{$assetGroup->name}}</option>
                @endforeach
            </select>
            <div class="invalid-feedback">{{ $errors->first('assetGroup') }}</div>
        </div>
        <div class="form-group">
            <label for="fileInput">Soubor</label>
            <input type="file" name="asset" class="form-control-file {{$errors->has('asset') ? 'is-invalid' : '' }}"
                   id="fileInput">
            <div class="invalid-feedback">{{ $errors->first('asset') }}</div>
        </div>
        <button type="submit" class="btn btn-primary">Přidat</button>
    </form>
@endsection
