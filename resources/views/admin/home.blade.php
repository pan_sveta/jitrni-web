@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - O nás')

@section('head')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>

@endsection

@section('content')
    <form action="/file-upload" class="dropzone" id="my-awesome-dropzone">
        @csrf
        <select name="" id="">
            <option value="a">a</option>
            <option value="b">b</option>
        </select>
    </form>
@endsection
