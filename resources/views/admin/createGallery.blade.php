@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - Vytvořit galerie')

@section('content')
    <h2>Vytvořit galerii</h2>
    <form method="post" action="{{action('GalleryController@store')}}" class="needs-validation">
        @csrf
        <div class="form-group">
            <label for="nameInput">Název</label>
            <input type="text" name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : '' }}" id="nameInput" aria-describdby="name" value="{{old('name')}}" required maxlength="255">
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        </div>
        <div class="form-group">
            <label for="urlNameInput">Url název (např. 'moje-galerie')</label>
            <input type="text" name="urlName" class="form-control {{$errors->has('urlName') ? 'is-invalid' : '' }}" id="urlNameInput" aria-describdby="urlName" value="{{old('urlName')}}" required maxlength="64" pattern="^[a-z\-\d]+$">
            <div class="invalid-feedback">{{ $errors->first('urlName') }}</div>
        </div>
        <div class="form-group">
            <label for="descriptionInput">Popis</label>
            <textarea name="description" class="form-control {{$errors->has('description') ? 'is-invalid' : '' }}" id="descriptionInput" aria-describdby="description" maxlength="5000" required>
                {{old('description')}}
            </textarea>
            <div class="invalid-feedback">{{ $errors->first('description') }}</div>
        </div>
        <button type="submit" class="btn btn-primary">Vytvořit</button>
    </form>
@endsection
