@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - Novinky')

@section('content')
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Název</th>
            <th scope="col">URL název</th>
            <th scope="col">Vytvořeno</th>
            <th scope="col">Upraveno</th>
            <th scope="col">Akce</th>
        </tr>
        </thead>
        <tbody>
        @foreach($galleries as $gallery)
            <tr>
                <th scope="row">{{$gallery->id}}</th>
                <td>{{$gallery->name}}</td>
                <td>{{$gallery->url_name}}</td>
                <td>{{$gallery->created_at->format('d.m. Y')}} ({{$gallery->created_at->diffForHumans()}})</td>
                <td>{{$gallery->updated_at->format('d.m. Y')}} ({{$gallery->created_at->diffForHumans()}})</td>
                <td>
                    <a class="btn btn-primary" href="{{action('GalleryController@show',$gallery)}}">Náhled</a>
                    <a class="btn btn-secondary" href="{{action('GalleryController@edit',$gallery)}}">Upravit</a>
                    <a class="btn btn-success" href="{{action('GalleryController@fill',$gallery)}}">Nahrát</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
