@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - Novinky')

@section('content')
<div class="row">
    @foreach($images as $image)
        <div class="col-4">
            <img class="img-fluid" src="{{Storage::url($image->url)}}" alt="">
        </div>
    @endforeach
</div>
@endsection
