@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - Vytvořit novinku')

@section('head')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({
            selector: '#contentTextarea',
            plugins: "image imagetools",
            images_upload_url: '/administrace/novinky/nahrat-obrazek',
            images_upload_base_path: '',
            images_upload_credentials: true,
            height: '500',
    });
    </script>
@endsection

@section('content')
    <h2>Vytvořit novinku</h2>
    <form method="post" action="{{action('PostController@store')}}" class="needs-validation">
        @csrf
        <div class="form-group">
            <label for="titleInput">Titulek</label>
            <input type="text" name="title" class="form-control {{$errors->has('title') ? 'is-invalid' : '' }}" id="titleInput" aria-describdby="title" value="{{old('title')}}" required max="50">
            <div class="invalid-feedback">{{ $errors->first('title') }}</div>
        </div>
        <div class="form-group">
            <label for="contentTextarea">Obsah novinky</label>
            <textarea id="contentTextarea" name="xcontent">{{old('xcontent')}}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Vytvořit</button>
    </form>
@endsection
