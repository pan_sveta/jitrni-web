@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - Přidat skupinu souborů')

@section('content')
    <h2>Vytvořit skupinu souborů</h2>
    <form method="post" action="{{action('AssetGroupController@store')}}" class="needs-validation">
        @csrf
        <div class="form-group">
            <label for="nameInput">Jméno skupiny</label>
            <input type="text" name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : '' }}" id="nameInput" aria-describdby="name" value="{{old('name')}}" required max="50">
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        </div>
        <button type="submit" class="btn btn-primary">Vytvořit</button>
    </form>
@endsection
