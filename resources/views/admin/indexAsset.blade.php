@extends('layouts.admin')
@section('title', 'ZŠ Jitřní - Novinky')

@section('content')
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Název</th>
            <th scope="col">Skupina souborů</th>
            <th scope="col">Vytvořeno</th>
            <th scope="col">Upraveno</th>
            <th scope="col">Akce</th>
        </tr>
        </thead>
        <tbody>
        @foreach($assets as $asset)
            <tr>
                <th scope="row">{{$asset->id}}</th>
                <td>{{$asset->name}}</td>
                <td>{{$asset->asset_group}}</td>
                <td>{{$asset->created_at->format('d.m. Y')}} ({{$asset->created_at->diffForHumans()}})</td>
                <td>{{$asset->updated_at->format('d.m. Y')}} ({{$asset->created_at->diffForHumans()}})</td>
                <td>
                    <a class="btn btn-primary" href="{{action('AssetController@show',$asset)}}">Stáhnout</a>
                    <a class="btn btn-danger" href="{{action('AssetController@destroy',$asset)}}">Odstranit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
