@extends('layouts.app')
@section('title', 'ZŠ Jitřní - Galerie')

@section('head')
    <link href="{{asset('css/lightbox.min.css')}}" rel="stylesheet">
    <script src="{{asset('js/lightbox-plus-jquery.min.js')}}"></script>
@endsection

@section('content')
    <h1>{{$gallery->name}}</h1>
    <p>{{$gallery->description}}</p>
    <div class="row">
        @foreach($images as $image)
            <div class="col-lg-3 col-md-4 col-xs-6 mb-4 gallery-tile d-flex justify-content-center align-items-center">
                <a href="{{Storage::url($image->url)}}" data-lightbox="{{$gallery->name}}"
                   data-title="{{$image->name}}">
                    <img class="img-thumbnail" src="{{Storage::url($image->url)}}" alt="{{$image->name}}">
                </a>
            </div>
        @endforeach
    </div>
@endsection
