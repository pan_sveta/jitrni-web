@extends('layouts.app')
@section('title', 'ZŠ Jitřní - Galerie')
@section('content')
<div class="row">
    <div class="col-12">
        @foreach($galleries as $gallery)
            <div class="card mb-3">
                <div class="card-img-top gallery-card-top">
                    @if(isset($gallery->images()->first()->url))
                        <img src="{{Storage::url($gallery->images()->first()->url)}}" alt="Card image cap">
                        @else
                    @endif
                </div>
                <div class="card-body">
                    <h5 class="card-title"><a href="{{action('PublicController@gallery',$gallery)}}">{{$gallery->name}}</a></h5>
                    <p class="card-text">{{$gallery->description}}</p>
                    <p class="card-text"><small class="text-muted">Přidáno: {{$gallery->created_at->diffForHumans()}}</small></p>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
