@extends('layouts.app')
@section('title', 'ZŠ Jitřní - Kontakty')
@section('head')
    <script src="https://api.mapy.cz/loader.js"></script>
    <script>Loader.load()</script>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-md-6">
            <h2 class="display-4 text-primary">Adresa</h2>
            <p>Základní škola s rozšířenou výukou tělesné výchovy Praha 4, Jitřní 185, příspěvková organizace</p>
            <p>Jitřní 185/6, 147 00 Praha 4 - Hodkovičky</p>
            <br>
            <p><strong class="text-secondary">Telefon:</strong> +420 244 466 550</p>
            <p><strong class="text-secondary">Datová schránka:</strong> aqmk9td</p>
            <p><strong class="text-secondary">E-mail:</strong> skola@zsjitrni.cz</p>
        </div>
        <div class="col-12 col-md-6">
            <div id="mapa"></div>
            <script type="text/javascript">
                var center = SMap.Coords.fromWGS84(14.413540, 50.024280);
                var m = new SMap(JAK.gel("mapa"), center, 13);
                m.addDefaultLayer(SMap.DEF_BASE).enable();
                m.addDefaultControls();

                var layer = new SMap.Layer.Marker();
                m.addLayer(layer);
                layer.enable();

                var options = {};
                var marker = new SMap.Marker(center, "myMarker", options);
                layer.addMarker(marker);

                var sync = new SMap.Control.Sync({bottomSpace: 30});
                m.addControl(sync);
            </script>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12 col-md-5">
            <h2 class="display-4 text-primary">Telefoní kontakty</h2>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Osoba</th>
                    <th scope="col">Telefoní číslo</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Kancelář školy</td>
                    <td><a href="tel:+420244466550">244 466 550</a></td>
                </tr>
                <tr>
                    <td>Vrátnice</td>
                    <td><a href="tel:+420244466552">244 466 552</a></td>
                </tr>
                <tr>
                    <td>Ředitelka ZŠ Mgr. Milena Hartigová</td>
                    <td><a href="tel:+420244466551">244 466 551</a></td>
                </tr>
                <tr>
                    <td>Zástupce ŘZŠ Mgr. Jiří Barták</td>
                    <td><a href="tel:+420244466546">244 466 546</a></td>
                </tr>
                <tr>
                    <td>Zástupkyně ŘZŠ Mgr. Sylva Bartková</td>
                    <td><a href="tel:+420244466548">244 466 548</a></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 col-md-5 offset-md-2">
            <h2 class="display-4 text-primary">E-mail</h2>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Osoba</th>
                    <th scope="col">Emailová adressa</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Kancelář školy</td>
                    <td><a href="mailto:skola@zsjitrni.cz">skola@zsjitrni.cz</a></td>
                </tr>
                <tr>
                    <td>Jídelna</td>
                    <td><a href="mailto:obedy@zsjitrni.cz">obedy@zsjitrni.cz</a></td>
                </tr>
                <tr>
                    <td>Učitelé</td>
                    <td>jmeno.prijmeni@zsjitrni.cz</td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
@endsection
