@extends('layouts.app')
@section('title', 'ZŠ Jitřní - '.$event->name)
@section('content')

    <div class="row">
        <div class="col-10 offset-1">
            <h1 class="text-primary">{{$event->name}}</h1>
            <p class="lead">
                Datum konání: {{\Carbon\Carbon::parse($event->date)->formatLocalized('%d. %B %Y')}}

            </p>
            <p class="lead">
                Datum konání: {{\Carbon\Carbon::parse($event->time)->format('h:i')}}
            </p>
            <div class="card">
                <div class="card-body">
                    <div class="card-text">
                        {!! $event->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
