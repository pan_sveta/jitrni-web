@extends('layouts.app')
@section('title', 'ZŠ Jitřní - '.$post->title)
@section('content')

    <div class="row">
        <div class="col-10 offset-1">
            <h1 class="text-primary">{{$post->title}}</h1>
            <p class="lead">
                {{\Carbon\Carbon::parse($post->created_at)->formatLocalized('%d. %B %Y')}}
            </p>
            <div class="card">
                <div class="card-body">
                    <div class="card-text">
                        {!! $post->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
