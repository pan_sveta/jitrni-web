@extends('layouts.app')
@section('title', 'ZŠ Jitřní - Ke stažení')
@section('content')
    <h2 class="display-4 text-primary">Ke stažení</h2>
    <div class="row">
        @foreach($assetGroups as $assetGroup)
            <div class="col-12 col-md-4 mt-3 mt-md-0">
                <h3 class="text-secondary">{{$assetGroup->name}}</h3>
                <div class="list-group">
                   @foreach($assetGroup->assets as $asset)
                        <a href="{{action('AssetController@show',$asset)}}" class="list-group-item list-group-item-action">
                            {{$asset->name}}
                        </a>
                       @endforeach
                </div>
            </div>
        @endforeach
    </div>
@endsection
