@extends('layouts.app')
@section('title', 'Základní škola Jitřní')
@section('content')
    <div class="row">
        <div class="col-12">
            <h1 class="text-primary text-center">Základní škola Jitřní s rozšířenou výukou tělesné výchovy</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="img/vchod.jpg" class="d-block w-100" alt="Vchod">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Moderní škola v srdci Hodkoviček</h5>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="img/stara-trida.jpg" class="d-block w-100" alt="Stará třída">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Škola s dlouholetou tradicí</h5>
                            <p>Založena v roce 1938.</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="img/sportoviste.jpg" class="d-block w-100" alt="Stará třída">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Rozšířená výuka tělesné výchovy</h5>
                            <p>Moderní a vybavená sportovičtě. Nabídkka kroužků.</p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12 col-md-6">
            <h2>Aktuality</h2>
            @foreach($posts as $post)
                <div class="card mb-3">
                    <div class="card-body">
                        <a href="{{action('PublicController@actuality',$post)}}"><h5 class="card-title text-primary">{{$post->title}}</h5></a>
                        <p class="card-text">{{substr(Html2Text\Html2Text::convert($post->content),0,255)}}...</p>
                    </div>
                </div>
                @endforeach
        </div>
        <div class="col-12 col-md-6">
            <h2>Akce</h2>
            @foreach($events as $event)
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-5">
                                <time datetime="{{$event->date}}" class="icon">
                                    <em>{{\Carbon\Carbon::parse($event->date)->formatLocalized('%A')}}</em>
                                    <strong>{{\Carbon\Carbon::createFromFormat('Y-m-d',$event->date)->formatLocalized('%B')}}</strong>
                                    <span>{{\Carbon\Carbon::parse($event->date)->format('d')}}</span>

                                </time>
                            </div>
                            <div class="col-7">
                                <h5><a href="{{action('PublicController@event',$event)}}">{{$event->name}}</a></h5>
                                <p class="card-text">{{substr(Html2Text\Html2Text::convert($event->content),0,128)}}...</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
