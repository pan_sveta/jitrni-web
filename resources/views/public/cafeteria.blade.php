@extends('layouts.app')
@section('title', 'ZŠ Jitřní - Školní jídelna')
@section('content')
    <div class="row">
        <div class="col-12">
            <h1 class="display-4 text-primary">Školní jídelna</h1>
            <h2 class="text-danger">Upozornění</h2>
            <ol class="text-danger">
                <li>Přihlašování jídel a změna menu - do 14.00 hod. tři pracovní dny předem. Tedy např. na čtvrtek nejpozději v pondělí, na pondělí ve středu předchozí týden.</li>
                <li>Odhlašování jídel - do 14.00 předchozí den</li>
            </ol>
            <br>
            <a href="https://e-jidelnicek.cz/menu/?canteenNumber=111470"><img class="d-block mx-auto img-fluid" alt="Logo e-jídelníček" src="img/e-jidelnicek-logo.png"></a>
        </div>
    </div>

@endsection
