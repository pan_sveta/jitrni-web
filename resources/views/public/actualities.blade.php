@extends('layouts.app')
@section('title', 'ZŠ Jitřní - Aktuality')
@section('content')
    <div class="row">
        <div class="col-12 col-md-8">
            <h2 class="display-4 text-primary">Aktuality</h2>
            @foreach($posts as $post)
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title">
                            <a href="{{action('PublicController@actuality',$post)}}">{{$post->title}}</a>
                            <small class="float-right">{{\Carbon\Carbon::parse($post->created_at)->format('d. m. Y')}}</small>
                        </h5>
                        <hr>
                        <p class="card-text">{{substr(Html2Text\Html2Text::convert($post->content),0,512)}} <a href="">...</a></p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    {{$posts->links()}}
    <hr>
    <div class="row">
        <div class="col-12 offset-md-4 col-md-8">
            <h2 class="display-4 text-secondary">Akce</h2>
            @foreach($events as $event)
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-3">
                                <time datetime="{{$event->date}}" class="icon">
                                    <em>{{\Carbon\Carbon::parse($event->date)->format('l')}}</em>
                                    <strong>{{\Carbon\Carbon::parse($event->date)->format('F')}}</strong>
                                    <span>{{\Carbon\Carbon::parse($event->date)->format('d')}}</span>
                                </time>
                            </div>
                            <div class="col-9">
                                <h5>
                                    <a href="{{action('PublicController@event',$event)}}">{{$event->name}}</a>
                                    <small class="float-right">{{\Carbon\Carbon::parse($post->created_at)->format('d. m. Y')}}</small>
                                </h5>

                                <hr>
                                <p class="card-text">{{substr(Html2Text\Html2Text::convert($event->content),0,255)}} <a href="">...</a></p>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        @endforeach
        {{$events->links()}}
    </div>
    </div>
@endsection
