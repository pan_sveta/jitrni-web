@extends('layouts.app')
@section('title', 'ZŠ Jitřní - Jitřní v číslech')
@section('head')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/countup.js/1.9.3/countUp.min.js"></script>

    <style>
        .countersWrapper{
            position: relative;
            height: 40rem;
        }
        .studentCountWrapper{
            position: absolute;
            font-size: 5rem;
            top: 50%;;
            left: 25%;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <h1 class="display-4">Jitřní v číslech</h1>
            <div class="countersWrapper">
                <div class="studentCountWrapper">
                    <span id="studentCount"></span><span> žáků</span>
                </div>
            </div>
        </div>
    </div>

    <script>
        var options = {
            useEasing: true,
            useGrouping: true,
            separator: ',',
            decimal: '.',
        };
        var studentCount = new CountUp('studentCount', 0, 5138, 0, 2.5, options);

        studentCount.start()
    </script>
@endsection
