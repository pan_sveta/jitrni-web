@extends('layouts.app')
@section('title', 'ZŠ Jitřní - Kariéra')
@section('content')
<div class="row">
    <div class="col-12">
        <h1 class="display-4 text-primary">Hledáme</h1>
        <ul class="list-group">
            <li class="list-group-item">Učitele/učitelku pro 1. stupeň</li>
            <li class="list-group-item">Učitele/učitelku matematiky (částečný úvazek)</li>
            <li class="list-group-item">Učitele/učitelku tělesné výchovy (částečný úvazek)</li>
            <li class="list-group-item">Řidiče/řidičku na dovoz obědů (částečný úvazek nebo DPP)</li>
        </ul>
        <br>
        <p class="lead">Nástup možný dle dohody.</p>
    </div>
</div>
@endsection
