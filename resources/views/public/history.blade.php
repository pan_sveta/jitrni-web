@extends('layouts.app')
@section('title', 'ZŠ Jitřní - Historie')
@section('content')
    <div class="row">
        <div class="col-0 col-sm-2">
            <div class="timeline d-none d-sm-block">
                <span>1938</span>
            </div>
        </div>
        <div class="col-12 col-sm-10">
            <h2 class="text-primary">Rozjezd - prvních cca 20 let školy (1938 až 1960)</h2>
            <p>V roce 1938 se Hodkovičky dočkaly své školy. Tehdy se ještě neučilo na Jitřní, ani
                současná budova školy nestála.</p>
            <p>Na místě, kde škola stojí dnes, byly jen louky, pole a les. Učilo se v několika tzv.
                „pavilonech“ (podobné jako ve filmu „Obecná škola“). Ty stály v prostoru křižovatky
                ulic Klánova – Havlovického (nyní je zde louka a bytové domy). Pavilony měly
                nedostatečnou kapacitu, proto výuka probíhala i střídavě – některé třídy dopoledne,
                jiné odpoledne.</p>
            <p>Postupně pavilony doplnilo několik tříd ve vilách v ulici Havlovického.</p>
            <hr class="my-5">
        </div>
    </div>
    <div class="row">
        <div class="col-0 col-sm-2">
            <div class="timeline d-none d-sm-block">
                <span>1960</span>
            </div>
        </div>
        <div class="col-12 col-sm-10">
            <h2 class="text-primary">Nová škola - dalších 25 let školy (1960 až 1985)</h2>
            <p>V roce 1960 se žáci ze všech pavilonů a vil přesunuli do nově postavené školy v ulici
                Jitřní. Prázdné pavilony byly strženy, vily prodány a přestavěny na byty.</p>
            <p>Školní areál tehdy tvořily dvě samostatné budovy – škola s tělocvičnami a budova
                jídelny. V přízemí jídelny byly školní dílny, ve kterých se vyučovaly technické práce
                (prostory dnešní družiny). Tělocvičny byly tehdy jen dvě, současná modrá a zelená.</p>
            <p>Několik let po otevření školy se polovina z dvaceti tříd stěhuje i se svými učiteli do nově
                postavené školy v ulici Jílovská. Poloprázdná škola doplňuje svou kapacitu tím, že je zde
                umístěna Střední knihovnická škola. Postupně se i díky okolní výstavbě počet žáků
                zvyšoval, kapacita se naplnila a Střední knihovnická škola odchází do jiných prostor.</p>
            <hr class="my-5">
        </div>
    </div>
    <div class="row">
        <div class="col-0 col-sm-2">
            <div class="timeline d-none d-sm-block">
                <span>1985</span>
            </div>
        </div>
        <div class="col-12 col-sm-10">
            <h2 class="text-primary">Sportovní rozšíření – od roku 1985 až dodnes</h2>
            <p>Díky kvalitnímu školnímu sportovnímu areálu mohly v roce 1985 vzniknout první
                specializované sportovní třída. Rozvoj sportu pokračoval a ZŠ Jitřní získala přídomek "s
                rozšířenou výukou tělesné výchovy". V roce 1994 byla uvedena do provozu nová,
                v pořadí již třetí, tělocvična. Spolu s ní vznikl uzavřený koridor propojující školu,
                tělocvičny a jídelnu.</p>
            <p>V roce 1996 odstartovalo pod názvem Laťka Jitřní historicky první klání ve skoku
                vysokém. Tato sportovní akce je každoročně připravována pro desítky závodníků jak z
                Jitřní, tak i z jiných škol.</p>
            <p>Sportovnímu zaměření školy byly přizpůsobeny venkovní sportoviště. Atletický ovál
                získal tartanovou dráhu, vznikly specializované atletické sektory. V roce 2012 byla
                dobudována i dvě nová multifunkční hřiště.</p>
            <p>Naši žáci mají za sebou národní i mezinárodní úspěchy.</p>
            <hr class="my-5">
        </div>
    </div>
    <div class="row">
        <div class="col-0 col-sm-2">
            <div class="timeline d-none d-sm-block">
                <span>2008</span>
            </div>
        </div>
        <div class="col-12 col-sm-10">
            <h2 class="text-primary">Spojení dvou škol (2008 až 2018)</h2>
            <p>V letech 2008 až 2018 byla ZŠ Jitřní administrativně spojena se ZŠ Filosofská. Vzdálenost
                mezi budovami obou objektů (cca 1,5 km) i odlišná struktura žáků však působily
                komplikace. Zřizovatel (Městská část Praha 4) v září roku 2017 rozhodl o opětovném
                osamostatnění obou škol. K němu došlo 1. září 2018.</p>
            <hr class="my-5">
        </div>
    </div>
    <div class="row">
        <div class="col-0 col-sm-2">
            <div class="timeline d-none d-sm-block">
                <span>2009</span>
            </div>
        </div>
        <div class="col-12 col-sm-10">
            <h2 class="text-primary">Zásadní rekonstrukce budovy i sportovního areálu (2009 až 2010)</h2>
            <p>Příchodem školního roku 2008/2009 se škola se začíná připravovat na zásadní
                rekonstrukci. Na Jitřní se od 1. září 2008 vyučují jen žáci 6. až 9. ročníku, žáci 1. až 5.
                ročníků jsou dočasně přestěhováni do učeben ve škole v ul. Filosofská. Od podzimních
                prázdnin pak utichá školní život zcela. I žáci 6. až 9. ročníků se stěhují do „náhradních“
                prostor. Ty poskytuje škola v ul. Na Planině. Do náhradních prostor jsou žáci
                přepravováni speciálními školními autobusy.</p>
            <p>Na Jitřní se žáci vrací 1. 2. 2010. Rekonstrukce ale ještě v té době nebyla plně
                dokončena. Na řadu přichází celý venkovní areál. Jsou vykáceny prakticky všechny
                stromy, dochází k přebudování svahu spojující horní a spodní část sportovního areálu a
                k rozsáhlým parkovým úpravám. Kromě toho zde vznikají dvě nová multifunkční hřiště
                s tartanem a umělou travou. Škola Jitřní tak získala ještě lepší podmínky pro své
                sportovní zaměření.</p>
        </div>
    </div>
@endsection
