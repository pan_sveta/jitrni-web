<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('favicon/safari-pinned-tab.svg" color="#5bbad5') }}">
    <link rel="shortcut icon" href="{{ asset('favicon/favicon.ico') }}">
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="msapplication-config" content="{{ asset('favicon/browserconfig.xml') }}">
    <meta name="theme-color" content="#ffffff">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @yield('head')
</head>
<body class="public-body">
    <div class="container" id="mainContainer">
        <nav class="navbar navbar-expand-lg navbar-light bg-light mb-4">
            <a class="navbar-brand" href="{{action('PublicController@index')}}"><img src="/img/logo.png" width="53" height="30" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                        <a class="nav-link" href="{{action('PublicController@index')}}">Domů</a>
                    </li>
                    <li class="nav-item {{ Request::is('aktuality') ? 'active' : '' }}">
                        <a class="nav-link" href="{{action('PublicController@actualities')}}">Aktuality</a>
                    </li>
                    <li class="nav-item {{ Request::is('historie') ? 'active' : '' }}">
                        <a class="nav-link" href="{{action('PublicController@history')}}">Historie</a>
                    </li>
                    <li class="nav-item {{ Request::is('galerie') ? 'active' : '' }}">
                        <a class="nav-link" href="{{action('PublicController@galleries')}}">Galerie</a>
                    </li>
                    <li class="nav-item {{ Request::is('kontakty') ? 'active' : '' }}">
                        <a class="nav-link" href="{{action('PublicController@contact')}}">Kontakty</a>
                    </li>
                    <li class="nav-item {{ Request::is('ke-stazeni') ? 'active' : '' }}">
                        <a class="nav-link" href="{{action('PublicController@downloads')}}">Ke stažení</a>
                    </li>
                    <li class="nav-item {{ Request::is('skolni-jidelna') ? 'active' : '' }}">
                        <a class="nav-link" href="{{action('PublicController@cafeteria')}}">Školní jídelna</a>
                    </li>
                    <li class="nav-item {{ Request::is('kariera') ? 'active' : '' }}">
                        <a class="nav-link" href="{{action('PublicController@career')}}">Kariéra u nás</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="https://skola.zsjitrni.cz/bakaweb/next/login.aspx">
                            <img class="d-inline-block" height="20" src="{{asset("img/bakalari-logo.png")}}" alt="Bakaláži logo">
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        @yield('content')
    </div>
</body>
</html>
