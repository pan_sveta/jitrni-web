<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('favicon/safari-pinned-tab.svg" color="#5bbad5') }}">
    <link rel="shortcut icon" href="{{ asset('favicon/favicon.ico') }}">
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="msapplication-config" content="{{ asset('favicon/browserconfig.xml') }}">
    <meta name="theme-color" content="#ffffff">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">

    @yield('head')
</head>
<body>
<div>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">ZŠ Jitřní - Administrace</a>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="{{ route('logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    {{ __('Odhlásit se') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                        <span>Možnosti</span>
                        <a class="d-flex align-items-center text-muted" href="#">
                        </a>
                    </h6>
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('administrace/novinky') ? 'active' : '' }}"
                               href="{{action('PostController@index')}}">Novinky</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('administrace/novinky/nova') ? 'active' : '' }}"
                               href="{{action('PostController@create')}}">Vytvořit novinku</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('administrace/akce') ? 'active' : '' }}"
                               href="{{action('EventController@index')}}">Akce</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('administrace/akce/nova') ? 'active' : '' }}"
                               href="{{action('EventController@create')}}">Vytvořit akci</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('administrace/galerie') ? 'active' : '' }}"
                               href="{{action('GalleryController@index')}}" href="#">Galerie</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('administrace/galerie/nova') ? 'active' : '' }}"
                               href="{{action('GalleryController@create')}}">Vytvořit galerie</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('administrace/skupiny-souboru') ? 'active' : '' }}"
                               href="{{action('AssetGroupController@index')}}">Skupiny souborů</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('administrace/skupiny-souboru/nova') ? 'active' : '' }}"
                               href="{{action('AssetGroupController@create')}}">Vytvořit skupinu souborů</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('administrace/soubory') ? 'active' : '' }}"
                               href="{{action('AssetController@index')}}">Soubory</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('administrace/soubory/novy') ? 'active' : '' }}"
                               href="{{action('AssetController@create')}}">Přidat soubor</a>
                        </li>
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                @yield('content')
            </main>
        </div>
    </div>
</div>
</body>
</body>
</html>
