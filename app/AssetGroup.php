<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetGroup extends Model
{
    public function assets()
    {
        return $this->hasMany('App\Asset');
    }
}
