<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['gallery_id','name','url'];

    public function gallery()
    {
        return $this->belongsTo('App\Gallery');
    }
}
