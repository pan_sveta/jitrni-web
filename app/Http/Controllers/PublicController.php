<?php

namespace App\Http\Controllers;

use App\Asset;
use App\AssetGroup;
use App\Event;
use App\Gallery;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PublicController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('created_at', 'DESC')->take(3)->get();
        $events = Event::orderBy('date', 'DESC')->take(3)->get();
        return view('public.index')->with('posts', $posts)->with('events', $events);
    }

    public function history()
    {
        return view('public.history');
    }

    public function numbers()
    {
        return view('public.numbers');
    }

    public function actualities()
    {
        $posts = Post::orderBy('created_at', 'DESC')->paginate(3);
        $events = Event::orderBy('date', 'DESC')->paginate(3);
        return view('public.actualities')->with('posts', $posts)->with('events', $events);
    }

    public function actuality(Post $post)
    {
        return view('public.actuality')->with('post', $post);
    }

    public function event(Event $event)
    {
        return view('public.event')->with('event', $event);
    }

    public function ofSchoolLife()
    {
        return view('public.ofSchoolLife');
    }

    public function galleries()
    {
        return view('public.galleries')->with('galleries', Gallery::all());
    }

    public function gallery(Gallery $gallery)
    {
        return view('public.gallery')->with('gallery', $gallery)->with('images', $gallery->images);
    }

    public function contact()
    {
        return view('public.contact');
    }

    public function officialDesk()
    {
        return view('public.officialDesk');
    }

    public function downloads()
    {
        $assetGroups = AssetGroup::with('assets')->get();
        return view('public.downloads')->with('assetGroups', $assetGroups);
    }

    public function download(Asset $asset)
    {
        return Storage::download($asset->url, $asset->filename);
    }

    public function cafeteria()
    {
        return view('public.cafeteria');
    }

    public function career()
    {
        return view('public.career');
    }
}
