<?php

namespace App\Http\Controllers;

use App\AssetGroup;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;

class AssetGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assetGroups = AssetGroup::all();
        return view('admin.indexAssetGroup')->with('assetGroups', $assetGroups);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.createAssetGroup');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:50',
        ]);

        $assetGroup = new AssetGroup();
        $assetGroup->name = $request->name;
        $assetGroup->save();

        return redirect()->action('AssetGroupController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssetGroup $assetGroup
     * @return \Illuminate\Http\Response
     */
    public function show(AssetGroup $assetGroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssetGroup $assetGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(AssetGroup $assetGroup)
    {
        return view('admin.editAssetGroup', $assetGroup);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\AssetGroup $assetGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssetGroup $assetGroup)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:50',
        ]);

        $assetGroup->name = $request->name;
        $assetGroup->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssetGroup $assetGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetGroup $assetGroup)
    {
        $assetGroup->delete();
    }
}
