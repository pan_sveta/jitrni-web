<?php

namespace App\Http\Controllers;

use App\Asset;
use App\AssetGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AssetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets = Asset::orderBy('created_at', 'desc')->with('assetGroup')->get();
        return view('admin.indexAsset')->with('assets', $assets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $assetGroups = AssetGroup::orderBy('name')->get();
        return view('admin.createAsset')->with('assetGroups', $assetGroups);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:50',
            'assetGroup' => 'required|exists:asset_groups,id',
            'asset' => 'required',
        ]);
        $path = Storage::putFile("public/ke-stazeni", $request->asset);
        $asset = new Asset();
        $asset->name = $request->name;
        $asset->asset_group_id = $request->assetGroup;
        $asset->filename = $request->asset->getClientOriginalName();
        $asset->url = $path;
        $asset->save();

        return redirect(action('AssetController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        return Storage::download($asset->url, $asset->filename);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset $asset)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Asset $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset $asset)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
        Storage::delete($asset->url);
        $asset->delete();
        return back();
    }
}
